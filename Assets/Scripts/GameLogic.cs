using System.Collections;
using UnityEngine;

public class GameLogic : MonoBehaviour
{
    [SerializeField] private FirstDraggableObject _firstObject = null;
    [SerializeField] private SecondDraggableObject _secondObject = null;
    [SerializeField] private ThirdDraggableObject _thirdObject = null;

    [SerializeField] private SceneChanger _sceneChanger = null;
    private void OnEnable()
    {
        DraggableObject.objectPlaced += DraggableObjectjectPlaced;
    }
    
    private void OnDisable()
    {
        DraggableObject.objectPlaced -= DraggableObjectjectPlaced;
    }

    private void DraggableObjectjectPlaced(DraggableObject obj)
    {
        CheckIfWin();
    }

    private void CheckIfWin()
    {
        if (_firstObject.locked && _secondObject.locked && _thirdObject.locked)
        {
            StartCoroutine(LastSceneCor());
        }
    }

    private IEnumerator LastSceneCor()
    {
        yield return new WaitForSeconds(2f);
       _sceneChanger.LoadFinalScene();
    }
}
