using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour
{
   public void LoadCoreScene()
   {
      SceneManager.LoadScene(1);
   }
   
   public void LoadFinalScene()
   {
      SceneManager.LoadScene(2);
   }
}
