using System;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]

public abstract class DraggableObject : MonoBehaviour
{
    public static event Action<DraggableObject> objectPlaced = delegate { };
    
    [SerializeField] protected Transform _objectPlace = null;
    private Vector2 _initialPosition = Vector2.zero;
    private Vector3 _initialSize = Vector3.zero;

    private float _deltaX = 0;
    private float _deltaY = 0;

    public bool locked = false;

    private Collider2D _collider = null;

    private void Start()
    {
        _initialPosition = transform.position;
        _initialSize = transform.localScale;
        
        _collider = GetComponent<Collider2D>();
    }

    private void Update()
    {
        if (Input.touchCount > 0 && !locked)
        {
            Touch touch = Input.GetTouch(0);
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(touch.position);
    
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    if (_collider == Physics2D.OverlapPoint(touchPos))
                    {
                        transform.localScale = _objectPlace.localScale;
                        
                        _deltaX = touchPos.x - transform.position.x;
                        _deltaY = touchPos.y - transform.position.y;
                    }
                    break;
                
                case TouchPhase.Moved:
                    if (_collider == Physics2D.OverlapPoint(touchPos))
                    {
                        transform.position = new Vector2(touchPos.x - _deltaX, touchPos.y - _deltaY);
                    }
                    break;
                
                case TouchPhase.Ended:
                    if (Mathf.Abs(transform.position.x - _objectPlace.position.x) <= 0.5f &&
                        Mathf.Abs(transform.position.y - _objectPlace.position.y) <= 0.5f)
                    {
                        transform.position = new Vector2(_objectPlace.position.x, _objectPlace.position.y);
                        locked = true;
                        
                        objectPlaced(this);
                    }
                    else
                    {
                        transform.position = new Vector2(_initialPosition.x, _initialPosition.y);
                        transform.localScale = _initialSize;
                    }
                    break;
            }
        }
    }

}
